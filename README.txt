--------------------------------------------------------------------------------
                        Simple Subscription Rules
--------------------------------------------------------------------------------

Adds Rules support for the Simple Subscription module. Provides reaction events for:
- Subscription Create
- Subscription Update
- Subscription Delete

Project homepage: https://www.drupal.org/project/rules_simple_subscription

Installation
------------

*Before* starting, make sure that you have read at least the introduction - so
you know at least the basic concepts. You can find it here: https://www.drupal.org/node/298480

 * Simple Subscription Rules depends on the Simple Subscription module, download and install it from
   https://www.drupal.org/project/simple_subscription
 * Simple Subscription Rules depends on the Rules module, download and install it from
   https://www.drupal.org/project/rules
 * Rules depends on the Entity API module, download and install it from
   https://www.drupal.org/project/entity
 * Copy the whole rules directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate the Rules and Rules UI
   modules.
 * The administrative user interface can be found at admin/config/workflow/rules
